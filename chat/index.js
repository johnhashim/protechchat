
  var config = {
    apiKey: "AIzaSyCiIj48dgYTgAUZEwynnftUyHMz9VCOItg",
    authDomain: "johnhashim-206004.firebaseapp.com",
    databaseURL: "https://johnhashim-206004.firebaseio.com",
    projectId: "johnhashim-206004",
    storageBucket: "johnhashim-206004.appspot.com",
    messagingSenderId: "462033190757"
  }
  firebase.initializeApp(config)
  const database = firebase.database()
  const messagesRef = database.ref('messages')
  new Vue({
    el: "#chat",
    data: {
      messages: [],
      messageText: '',
      nickname: 'zach',
      editingMessage: null
    },
    methods: {
      storeMessage () {
        messagesRef.push({text: this.messageText, nickname: this.nickname})
        this.messageText = ''
      },
      deleteMessage (message) {
        messagesRef.child(message.id).remove()
      },
      editMessage (message) {
        this.editingMessage = message
        this.messageText = message.text
      },
      cancelEditing () {
        this.editingMessage = null
        this.messageText = ''
      },
      updateMessage () {
        messagesRef.child(this.editingMessage.id).update({text: this.messageText})
        this.cancelEditing()
      }
    },
    created () {
      // value = snapshot.val() | key = snapshot.key
      messagesRef.on('child_added', snapshot => {
        this.messages.push({...snapshot.val(), id: snapshot.key})
        if(snapshot.val().nickname !== this.nickname){
              nativeToast({
                message: `new massage by ${snapshot.val().nickname}`,
                position: 'top',
                timeout: 5000,
                type: 'success'
              })
        }
      })
      messagesRef.on('child_removed', snapshot => {
        const deletedMessage = this.messages.find(message => message.id === snapshot.key)
        const index = this.messages.indexOf(deletedMessage)
        this.messages.splice(index, 1)
        if(snapshot.val().nickname !== this.nickname){
              nativeToast({
                message: `massage deleted by ${snapshot.val().nickname}`,
                type: 'warning'
              })
        }
      })
      messagesRef.on('child_changed', snapshot => {
        const updatedMessage = this.messages.find(message => message.id === snapshot.key)
        updatedMessage.text = snapshot.val().text
        if(snapshot.val().nickname !== this.nickname){
          nativeToast({
            message: `massage edited by ${snapshot.val().nickname}`,
            type: 'info'
          })
        }
      })
    }
  })

 
